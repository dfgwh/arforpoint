﻿using UnityEngine;
using UnityEngine.UI;


public class MainPageController : MonoBehaviour
{
    #region Unity Editor

    [SerializeField] private Button logoutButton;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        logoutButton.onClick.AddListener(() =>
        {
            GameSession.QuitApp();
        });
    }

    #endregion
}
