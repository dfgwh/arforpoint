﻿using UnityEngine;
using Vuforia;

public class AudioController : MonoBehaviour
{

    #region Unity Editor

    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private GameObject _target;
    [SerializeField] private string _targetName;

    #endregion

    #region Private Fields

    private bool _isMusic = true;

    #endregion

    #region Unity Methods


    private void Start()
    {
        ARObjectsSwitchController.Instance.onSwichMagicObject += Instance_onSwichMagicObject;
        ARObjectsSwitchController.Instance.onSwichBearObject += Instance_onSwichBearObject;
    }

    private void OnDestroy()
    {
        ARObjectsSwitchController.Instance.onSwichMagicObject -= Instance_onSwichMagicObject;
        ARObjectsSwitchController.Instance.onSwichBearObject -= Instance_onSwichBearObject;
    }

    #endregion

    #region Private Methods

    private void Instance_onSwichBearObject()
    {
        throw new System.NotImplementedException();
    }

    private void Instance_onSwichMagicObject()
    {
        Debug.Log("<color=green>magic onSwitch</color>");

        if (!_isMusic)
        {
            _audioSource.Play();
            _isMusic = true;
        }
        else
        {
            _audioSource.Pause();
            _isMusic = false;
        }
    }

    #endregion

}
