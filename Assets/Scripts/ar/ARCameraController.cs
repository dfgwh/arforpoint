﻿using UnityEngine;
using Vuforia;

[RequireComponent(typeof(CameraFocus))]
public class ARCameraController : MonoBehaviour
{
    #region Properties

    public static ARCameraController Instance
    {
        get
        {
            return instance;
        }
        set
        {
            instance = value;
        }
    }

    #endregion

    #region Private Fields

    private static ARCameraController instance = null;
    private CameraFocus cameraFocus = null;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }

    private void Start()
    {
        cameraFocus = GetComponent<CameraFocus>();
        //StopAR();
    }

    #endregion

    #region Public Methods

    public void SetFocus()
    {
        cameraFocus.SetFocus();
    }

    public void StartAR()
    {
        CameraDevice.Instance.Start();
        cameraFocus.SetFocus();
    }

    public void StopAR()
    {
        CameraDevice.Instance.Stop();
    }

    #endregion
}
