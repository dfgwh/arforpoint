﻿using UnityEngine;

public class ARObjectsSwitchController : MonoBehaviour
{
    #region Properties

    public static ARObjectsSwitchController Instance
    {
        get
        {
            return instance;
        }
        set
        {
            instance = value;
        }
    }

    #endregion

    #region Private Fields

    private static ARObjectsSwitchController instance = null;

    #endregion

    #region Delegate Fields

    public delegate void ARObjectsSwitch();

    public event ARObjectsSwitch onSwichMagicObject;
    public event ARObjectsSwitch onSwichBearObject;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }

    #endregion

    #region Public Methods

    public void SwichMagicObj()
    {
        if (onSwichMagicObject != null) onSwichMagicObject();
    }

    #endregion

}
