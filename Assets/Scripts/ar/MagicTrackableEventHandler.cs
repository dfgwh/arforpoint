﻿using UnityEngine;

public class MagicTrackableEventHandler : DefaultTrackableEventHandler
{

    #region Protected Methods

    protected override void OnTrackingFound()
    {
        base.OnTrackingFound();
        ARObjectsSwitchController.Instance.SwichMagicObj();
    }

    protected override void OnTrackingLost()
    {
        base.OnTrackingLost();
        ARObjectsSwitchController.Instance.SwichMagicObj();
    }

    protected override void Start()
    {
        base.Start();
    }

    #endregion


}
