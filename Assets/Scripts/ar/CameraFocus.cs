﻿using Vuforia;
using UnityEngine;
using System.Collections;

public class CameraFocus : MonoBehaviour
{
    #region Methods

    #region Private Methods

    private void Start()
    {
        StartCoroutine(WaitVuforia());
    }

    private IEnumerator WaitVuforia()
    {
        while (!VuforiaARController.Instance.HasStarted)
        {
            yield return null;
        }

        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    private void AfterPause()
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    private void OnPause()
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_NORMAL);
    }

    #endregion

    #region Public Methods

    public void SetFocus()
    {
        if (!CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO))
        {
            CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_NORMAL);
        }
    }

    #endregion

    #region Unity Methods

    private void OnEnable()
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            AfterPause();
        }

        else
        {
            OnPause();
        }
    }

    private void OnApplicationFocus(bool focus)
    {
        if (focus)
        {
            AfterPause();
        }

        else
        {
            OnPause();
        }
    }

    #endregion

    #endregion
}